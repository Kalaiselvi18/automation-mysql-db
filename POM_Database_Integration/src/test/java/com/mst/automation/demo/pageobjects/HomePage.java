package com.mst.automation.demo.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mst.automation.demo.abstractclasses.DriverClass;
import com.mst.automation.demo.utils.SeleniumUtils;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Dec 12, 2017
 * Last Edited by: Aartheeswaran
 * Last Edited date: Dec 12, 2017
 * Description: Page objects for Homepage
 *
 */
public class HomePage extends DriverClass {

	public final By homeBy = By.cssSelector("a[title='Home Tab - Selected']");
	public final By accountBy = By.cssSelector("#Account_Tab>a");
	public final By contactBy = By.cssSelector("#Contact_Tab>a");
	public final By applicationFormBy = By.cssSelector(".wt-Application_Form>a");
	
	@FindBy(css = "#userNav-arrow")
	public WebElement userMenu;

	@FindBy(css = "a[title=Logout]")
	public WebElement logOut;

	public HomePage(WebDriver driver) {
		super(driver);
	}

	/*To click on the setup menu on the right side corner*/
	public LeadPage verifyHomePage() throws Exception {
		SeleniumUtils.elementToBeClickable(driver, homeBy);
		return new LeadPage(driver);
	}
		
	public ApplicationFormPage verifyApplicationTab() throws Exception {
		SeleniumUtils.elementToBeClickable(driver, applicationFormBy);
		return new ApplicationFormPage(driver);
	}

	/*To click the logout button*/
	public void logOutUser() throws Exception {
		SeleniumUtils.highLightElement(userMenu, driver);
		userMenu.click();
		logOut.click();
	}

}
