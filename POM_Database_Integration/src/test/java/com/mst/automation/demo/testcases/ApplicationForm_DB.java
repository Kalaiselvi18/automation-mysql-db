/**
 * @author Ashok Kumar Ganesan
 * Created date:
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.demo.testcases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mst.automation.demo.basetest.BaseTest;
import com.mst.automation.demo.extentreport.ReportGenerator;
import com.mst.automation.demo.utils.DatabaseUtility;
import com.mst.automation.demo.utils.TestUtils;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This class is used to fill the application form where the test data values
 * are fetched from the GoogleAPI.
 */
public class ApplicationForm_DB extends BaseTest{

	String firstName,lastName,middleName,gender,dob,maritalStatus,course,ugCourse, preferredCollege,verifyMessage;
	int listSize;
	static int k; 
	
	@Test(groups="Regression Suite",invocationCount=2)
	@Parameters({ "userType"})
	public void applicationFormCreation(String userType) throws Exception {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String tcName = "TC_DB001";

		String query = TestUtils.getStringFromPropertyFile(tcName);
		listOfLists = DatabaseUtility.getDBValues(query);
		list = DatabaseUtility.getDBMetadata(query);
		listSize = listOfLists.size();
		System.out.println(listSize);
		if(listSize>=1) {
			logger.info("********** "+tcName+" - "+methodName+" execution started**********");
			System.out.println(k);
			ArrayList<String> lol = listOfLists.get(k);
			System.out.println(lol);
			k++;
			applicationFormExecution(userType, methodName,lol);			
		}
		else {
			throw new Exception("No values are present under the database table. Check your database");
		}
	}

	public void applicationFormExecution(String userType, String methodName,ArrayList<String> listValues) throws IOException, Exception {
		String user =TestUtils.getStringFromPropertyFile(userType+".username");
		String password = TestUtils.getStringFromPropertyFile(userType+".password");
		
		extractListValues(listValues);
		
		reporter = new ReportGenerator(browser, methodName);
		logger.info("Logging with the following user : "+user+"");
		homePage = loginPage.login(user, password, reporter);
		logger.info("Successfully logged in as "+user+" user");
		
		logger.info("Verify the home page");
		applicationFormPage = homePage.verifyApplicationTab();
		logger.info("Verified the home page");	
		
		logger.info("Enter the personal information with valid test data");
		applicationFormPage.enterPersonalInformation_DB(reporter,firstName,lastName,middleName,gender,dob,maritalStatus,course);
		logger.info("Entered the personal information with valid test data");
		
		logger.info("Enter the UG course information with valid test data");
		applicationFormPage.enterUGCourseInformation_DB(reporter,ugCourse, preferredCollege);
		logger.info("Entered the UG course information with valid test data");
		
		logger.info("Choose the Day College option");
		applicationFormPage.selectDayCollege(reporter);
		logger.info("Chosen the day college option");
		
		logger.info("Click the save button");
		applicationFormPage.saveApplicationForm(reporter);
		logger.info("Clicked on the save button");
		
		logger.info("Verify the application form success message");
		applicationFormPage.verifyApplicationFormMessage_DB(reporter,verifyMessage);
		logger.info("Verified the application form success message");
	}

	public void extractListValues(ArrayList<String> listValues) {
		firstName = listValues.get(0);
		lastName = listValues.get(1);
		middleName = listValues.get(2);
		gender = listValues.get(3);
		dob = listValues.get(4);
		maritalStatus = listValues.get(5);
		course = listValues.get(6);
		ugCourse = listValues.get(7);
		preferredCollege = listValues.get(8);
		verifyMessage = listValues.get(9);
	}
}
