/**
 * @author Ashok Kumar Ganesan
 * Created date:
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.demo.listeners;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;

import org.testng.annotations.ITestAnnotation;
import org.testng.internal.annotations.ITest;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Mar 14, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
public class AnnotationTransformer implements IAnnotationTransformer{

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
		// TODO Auto-generated method stub
		if(testMethod.equals("contactCreation")) {
			System.out.println("test");
			annotation.setInvocationCount(2);
		}
	}
}
