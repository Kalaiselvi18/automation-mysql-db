package com.mst.automation.demo.constant;
/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: It contains the constant values which 
 * is used through out the project.
 */
public class Constant {
	
	private Constant() {
		
	}
	
	/*public static final String LINUXCHROMEDRIVER = System.getenv("CHROME_DRIVER");
	public static final String LINUXFIREFOXDRIVER = System.getenv("FIREFOX_DRIVER");
	
	// Binary location of chrome in the server
	public static final String CHROMEBINARY = System.getenv("CHROME_BINARY");
	public static final String FIREFOXBINARY = System.getenv("FIREFOX_BINARY");*/
	public static final String LINUXCHROMEDRIVER = "/opt/chromedriver";
	public static final String LINUXFIREFOXDRIVER = "/opt/geckodriver";
	
	// Binary location of chrome in the server
	public static final String CHROMEBINARY = "/opt/google/chrome/chrome";
	public static final String FIREFOXBINARY = "";
	public static final String WINDOWSCHROMEDRIVER = "src/test/resources/drivers/chromedriver.exe";
	public static final String WINDOWSFIREFOXDRIVER = "src/test/resources/drivers/geckodriver.exe";

	public static final String PROPERTYFILEPATH = "src/test/resources/test.properties";
	public static final String REPORTPATH = "Report/report.html";
	public static final String SCREENSHOTPATH = "Report/screenshot/";
	public static final String datafilePath = "src/test/resources/Testdata.xlsx";
	public static final String UPLOADFILE = "document/ica.exe";
	
    public static final String CHROMEDRIVER = "src/test/resources/drivers/chromedriver.exe";
    public static final String FIREFOXDRIVER = "src/test/resources/drivers/geckodriver.exe";
   
}
