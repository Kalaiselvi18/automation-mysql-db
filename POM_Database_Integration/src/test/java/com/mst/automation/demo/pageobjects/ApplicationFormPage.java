/**
 * @author Ashok Kumar Ganesan
 * Created date:
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.demo.pageobjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mst.automation.demo.abstractclasses.DriverClass;
import com.mst.automation.demo.customexception.CustomException;
import com.mst.automation.demo.extentreport.ReportGenerator;
import com.mst.automation.demo.utils.SeleniumUtils;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date:  Mar 20, 2018
 * Description: This class contains the ApplicationForm page objects and it's methods.
 */
public class ApplicationFormPage extends DriverClass {
	
	public final By ugCourseBy = By.xpath("html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[2]/div[1]/h3");

	public final By edit = By.cssSelector("input[name='edit']");
	
	public final By applicationFormBy = By.cssSelector(".wt-Application_Form>a");
	
	public final By eveCollegeValidationBy = By.cssSelector(".messageCell>div");
	
	@FindBy(how = How.CLASS_NAME, using = "wt-Application_Form")
	public WebElement applicationTab;
	
	@FindBy(how = How.CSS, using = "input[title='New']")
	public WebElement newButton;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td[1]/div/input")
	public WebElement firstName;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td[2]/input")
	public WebElement lastName;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[1]/input")
	public WebElement middleName;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[2]/select")
	public WebElement gender;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[3]/td[1]/div/span/input")
	public WebElement dob;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[3]/td[2]/select")
	public WebElement maritalStatus;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[4]/td[1]/input")
	public WebElement email;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[4]/td[2]/input")
	public WebElement mobile;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[5]/td[1]/textarea")
	public WebElement address;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[5]/td[2]/textarea")
	public WebElement interestedArea;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[1]/div/table/tbody/tr/td[1]/select")
	public WebElement course;
	
	@FindBy(how = How.XPATH, using = "html/body/div/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[2]/div[1]/h3")
	public WebElement ugCourse;
	
	@FindBy(how = How.XPATH, using = "html/body/div[1]/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[2]/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td[1]/select")
	public WebElement ugDegrees;
	
	@FindBy(how = How.XPATH, using = "html/body/div[1]/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[2]/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[1]/select")
	public WebElement preferredColleges;
	
	@FindBy(xpath = "html/body/div[1]/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[2]/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/a[1]/img")
	public WebElement selectionArrow;
	
	@FindBy(how = How.XPATH, using = "html/body/div[1]/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[2]/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[1]/input")
	public WebElement dayCollege;
	
	@FindBy(how = How.XPATH, using = "html/body/div[1]/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[2]/div[2]/table/tbody/tr/td/div/div/table/tbody/tr[3]/td[1]/input")
	public WebElement eveningCollege;
	
	@FindBy(how = How.XPATH, using = "html/body/div[1]/div[2]/table/tbody/tr/td/div[1]/form/div[1]/div/div/div/div/div[1]/span/div[3]/span/input[1]")
	public WebElement save;
	
	@FindBy(css=".dataCell>a")
	public WebElement formLink;
	
	@FindBy(css = ".messageText")
	public WebElement successMessage;
	
	@FindBy(css="input[name='edit']")
	public WebElement editButton;
	
	@FindBy(xpath=".//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a")
	public WebElement firstRecordLink;
	
	@FindBy(css=".messageCell>div")
	public WebElement eveCollegeValidationMessage;
	
	public ApplicationFormPage(WebDriver driver) {
		super(driver);
	}
	
	
	
	public void selectDayCollege(ReportGenerator reporter) {
		dayCollege.click();
		reporter.childReport("Chosen day college");
	}
	
	public void selectEveningCollege(ReportGenerator reporter) {
		eveningCollege.click();
		reporter.childReport("Chosen evening college");
	}
	
	public void saveApplicationForm(ReportGenerator reporter) throws InterruptedException {
		save.click();
		reporter.childReport("Clicked on Save button");
		Thread.sleep(5000);
	}
	
	public void presenceOfValidationMessage(ReportGenerator reporter) {
		// TODO Auto-generated method stub
		reporter.childReport("Verify the presence of validation message");
		SeleniumUtils.presenceOfValidationMessage(driver, eveCollegeValidationBy);
	}

	public void verifyApplicationFormMessage_DB(ReportGenerator reporter, String verifyMessage) throws InterruptedException {
		reporter.childReport("Verify the Application form success message ");
		SeleniumUtils.highlightElementBasedOnResult(successMessage, verifyMessage, driver);
		
	}

	public void enterUGCourseInformation_DB(ReportGenerator reporter, String ugCourses, String preferredCollege) throws InterruptedException {
		reporter.childReport("Verified the UG Course Header");
		SeleniumUtils.presenceOfElement(driver, ugCourseBy);
		SeleniumUtils.highLightElement(ugCourse, driver);
		
		reporter.childReport("Chosen UG course");
		SeleniumUtils.dropDown(ugDegrees,ugCourses);
		
		reporter.childReport("Chosen preferred colleges");
		SeleniumUtils.multiPicklist(preferredColleges,selectionArrow, preferredCollege);
	}

	public void enterPersonalInformation_DB(ReportGenerator reporter, String fn, String ln,
			String mn, String genderStatus, String dateOfBirth, String mStatus, String cours) throws InterruptedException {

		SeleniumUtils.highLightElement(applicationTab, driver);
		reporter.childReport("Application Tab clicked");
		applicationTab.click();
			
		reporter.childReport("New button clicked");
		newButton.click();
			
		Thread.sleep(5000);
		
		reporter.childReport("Entered first name");
		firstName.sendKeys(fn);
		
		reporter.childReport("Entered last name");
		lastName.sendKeys(ln);
		
		reporter.childReport("Entered middle name");
		middleName.sendKeys(mn);
		
		reporter.childReport("Chosen gender");
		SeleniumUtils.dropDown(gender, genderStatus);
		
		reporter.childReport("Entered DOB");
		dob.sendKeys(dateOfBirth);
		
		reporter.childReport("Chosen marital status");
		SeleniumUtils.dropDown(maritalStatus, mStatus);
		
		reporter.childReport("Entered the course");
		SeleniumUtils.dropDown(course, cours);	
	}
}
