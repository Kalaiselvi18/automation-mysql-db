package com.mst.automation.demo.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.mst.automation.demo.constant.Constant;
import com.mst.automation.demo.customexception.CustomException;

/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: This class contains the methods to load the property file, to compare the text.
 */
public final class TestUtils {
	
	private TestUtils() {
		
	}
	
	private static String userType;
	protected static final File file;
	protected static FileInputStream fileInput;
	protected static final Properties prop =  new Properties();
	
	static{
		file = new File(Constant.PROPERTYFILEPATH);
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw new CustomException("File not found" +e);
		}
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			throw new CustomException("IO Exception" +e);
		}
	}

	public static String getStringFromPropertyFile(String key){
		return prop.getProperty(key);
	}

		
	public static Integer getIntegerFromPropertyFile(String key){
		return new Integer(prop.getProperty(key));
	}
	
	public static String getUserType(){
		return userType;
	}

	public static void setUserType(String userTypeString){
		userType =userTypeString;
	}
	public static void compareText(String actual,String expected,WebElement element,WebDriver driver) throws InterruptedException {
		if(actual.equals(expected)) {
			SeleniumUtils.highLightElement(element, driver);
		}
		else {
			SeleniumUtils.highLightFailedElement(element, driver);
		}
	}

}

