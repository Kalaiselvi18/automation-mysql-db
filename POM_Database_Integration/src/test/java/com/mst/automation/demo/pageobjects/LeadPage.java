/**
 * @author Ashok Kumar Ganesan
 * Created date:
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description: 
 */
package com.mst.automation.demo.pageobjects;

import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.mst.automation.demo.abstractclasses.DriverClass;
import com.mst.automation.demo.extentreport.ReportGenerator;

import com.mst.automation.demo.utils.SeleniumUtils;

/**
 * @author Ashok Kumar Ganesan
 * Created date: Dec 12, 2017
 * Last Edited by: Infant Raja Marshall
 * Last Edited date: 
 * Description: Page objects and page methods for lead page.
 */
public class LeadPage extends DriverClass{

	public final By newButtonBy = By.cssSelector("input[title='New']");	
	
	public final By salutationBy = By.id("name_salutationlea2");
	
	public final By leadStatusBy = By.id("lea13");
	
	public final By accountNameBy = By.id("accid");
	
	public final By convertedStatusBy = By.id("cstatus");
	
	public final By deleteBy = By.cssSelector("input[title='Delete']");
	
	public final By leadTabBy = By.id("Lead_Tab");
	
	@FindBy(how = How.CSS, using = ".pageDescription")
	public WebElement pageDescription;
	
	@FindBy(how = How.ID, using = "Lead_Tab")
	public WebElement leadTab;

	@FindBy(how = How.CSS, using = "input[title='New']")
	public WebElement newButton;

	@FindBy(how = How.ID, using = "name_firstlea2")
	public WebElement firstname;

	@FindBy(how = How.ID, using = "name_lastlea2")
	public WebElement lastname;

	@FindBy(how = How.ID, using = "lea3")
	public WebElement company;

	@FindBy(how = How.ID, using = "lea16street")
	public WebElement street;

	@FindBy(how = How.ID, using = "lea16city")
	public WebElement city;

	@FindBy(how = How.ID, using = "lea16state")
	public WebElement state;

	@FindBy(how = How.ID, using = "lea16zip")
	public WebElement zip;

	@FindBy(how = How.ID, using = "lea16country")
	public WebElement country;

	@FindBy(how = How.XPATH, using = "html/body/div[1]/div[2]/table/tbody/tr/td[2]/form/div/div[1]/table/tbody/tr/td[2]/input[1]")
	public WebElement leadsave;

	@FindBy(how = How.CSS, using = ".btn[title='Delete']")
	public WebElement deleteButton;

	public final  By leadVerify = By.cssSelector("#lea2_ileinner");

	@FindBy(how = How.CSS, using = ".btn[title='Convert']")
	public WebElement convertButton;
	
	@FindBy(how = How.ID, using = "tsk5_fu")
	public WebElement taskName;
	 
	@FindBy(css = ".errorMsg")
	public WebElement error;
	
	@FindBy(css = "input[title='Cancel']")
	public WebElement cancel;
	 	 
	public LeadPage(WebDriver driver) {
		super(driver);
	}

	public void select_list(String salutation, By by) {
		Select element = new Select(driver.findElement(by));
		element.selectByVisibleText(salutation);
	}

	public WebElement getLeadName() throws Exception {
		WebElement element = null;
		try {

			element = driver.findElement(By.id("lea2_ileinner"));
		} catch (Exception E) {
		}
		return element;
	}
	
	public boolean isLeadTabDisplayed(ReportGenerator reporter) {
		boolean isLeadTabDisplayed = false;
		try {
			reporter.childReport("Verify presence of lead tab");
			WebElement element = driver.findElement(By.id("Lead_Tab"));
			isLeadTabDisplayed = element.isDisplayed();
		} catch (Exception ex) {
			throw ex;
		}
		return isLeadTabDisplayed;
	}

	public void alertAccept(WebDriver driver){

		Alert confirmationAlert = driver.switchTo().alert();
		String text = confirmationAlert.getText();
		System.out.println("Alert text is " + text);
		confirmationAlert.accept();
	}

	public void canceled() {
		try {
			cancel.click();
		}
		catch(Exception ex) {
			throw ex;
		}
	}
	
}
