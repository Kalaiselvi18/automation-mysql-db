package com.mst.automation.demo.pageobjects;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.mst.automation.demo.abstractclasses.DriverClass;
import com.mst.automation.demo.extentreport.ReportGenerator;
import com.mst.automation.demo.pageobjects.HomePage;

/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Dec 12, 2017
 * Last Edited by: Aartheeswaran
 * Last Edited date: Dec 12, 2017
 * Description: Page objects for login page
 */
	
public class LoginPage extends DriverClass {

	 @FindBy(how = How.ID, using = "username")
	 public WebElement txtbxUserName;

	 @FindBy(how = How.ID, using = "password")
	 public WebElement txtbxPassword;

	 @FindBy(how = How.ID, using = "Login")
	 public WebElement lnkLogin;
	 
	 public LoginPage(WebDriver driver){
		 super(driver);
		 PageFactory.initElements(driver, this);
	 }
	 
	 public void setUserName(String username){
		 txtbxUserName.clear();
		 txtbxUserName.sendKeys(username);
	 }
	 
	 public void setPassword(String password){
		 txtbxPassword.clear();
		 txtbxPassword.sendKeys(password);
	 }
	 
	 public void clickLogin(){
		 lnkLogin.click();
	 }
	 
	 /*To enter the credentials in the Login page*/
	 public HomePage login(String username, String password, ReportGenerator generator) throws IOException{
		 
		 setUserName(username);
		 generator.childReport("Username entered");
		 setPassword(password);
		 generator.childReport("Password entered");
		 clickLogin();
		 generator.childReport("Login clicked");
		 return new HomePage(driver);
	 }
}
