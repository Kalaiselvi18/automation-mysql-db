package com.mst.automation.demo.abstractclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: 
 * Description:This Driver Class is used to instatiate the Driver in all the 
 * page objects classes.
 */
public abstract class DriverClass  {

	protected WebDriver driver;
	protected WebDriverWait wait;
	protected Actions action;
	
	public DriverClass(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
}
