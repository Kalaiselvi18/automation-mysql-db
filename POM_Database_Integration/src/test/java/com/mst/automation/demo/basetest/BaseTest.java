package com.mst.automation.demo.basetest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.mst.automation.demo.browserfactory.BrowserFactory;
import com.mst.automation.demo.customexception.CustomException;
import com.mst.automation.demo.email.EmailReport;
import com.mst.automation.demo.extentreport.ReportGenerator;
import com.mst.automation.demo.pageobjects.LoginPage;

import com.mst.automation.demo.utils.TestUtils;
import com.mst.automation.demo.pageobjects.ApplicationFormPage;
import com.mst.automation.demo.pageobjects.HomePage;
import com.mst.automation.demo.pageobjects.LeadPage;

/**
 * 
 * @author Ashok Kumar Ganesan
 * Created date: Jan 5, 2018
 * Last Edited by: Ashok Kumar Ganesan
 * Last Edited date: April 4, 2018
 * Description: This Base class is used to run the before suite, after suite,
 * before method, after method in all the test methods.
 */

public class BaseTest {
	
	protected LoginPage loginPage;
	protected static HomePage homePage;
	protected LeadPage leadPage;
	protected ApplicationFormPage applicationFormPage;
	protected WebDriver driver;
	protected String url;
	protected String mBrowserType;
	protected String browser;
	protected String platform;
	protected String browserVersion;
	protected Map<String, List<String>> resultMap;
	protected List<String> results;
	protected ReportGenerator reporter;
	protected List<ArrayList<String>> listOfLists;
	protected ArrayList<String> list;
	protected static Logger logger = Logger.getLogger(BaseTest.class);
	
	@BeforeSuite (alwaysRun=true)
	/**
	 * This beforeSuite method call the openStream method for the workbook instance
	 * creation
	 */
	public void beforeSuite() throws IOException {
		PropertyConfigurator.configure("log4j.properties");
	}

	/**
	 * Setup method used to configure the browser, version, platform etc.,
	 * @param env
	 * @param userType
	 * @param browserType
	 * @throws Exception
	 */
	@BeforeMethod (alwaysRun=true)
	@Parameters({ "env", "userType", "browserType"})
	public void setup(String env, @Optional("userType") String userType, String browserType) {
		resultMap = new HashMap<>();
		results = new ArrayList<>();

		if (null != userType) {
			TestUtils.setUserType(userType);
		}
		mBrowserType = browserType;
		try {
			driver = BrowserFactory.loadDriver(mBrowserType.toUpperCase());
		} catch (Exception e) {
			logger.error(e);
		}

		url = TestUtils.getStringFromPropertyFile(env);
		
		browser = getBrowserName();
		browserVersion = getBrowserVersion();
		platform = getPlatform();

		driver.navigate().to(url);
		driver.manage().window().maximize();

		loginPage = new LoginPage(driver);
	}

	/**
	 * This method runs after every test which contains the logout, clear cookies.
	 * @param result
	 * @throws IOException 
	 * @throws Exception
	 */
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws IOException {

		String methodName = result.getMethod().getMethodName();
		if (result.getStatus() == ITestResult.FAILURE) {
			String res = result.getThrowable().getMessage();
			reporter.logScreenshot(driver, methodName, res);
		}
		else if(result.getStatus() == ITestResult.SKIP) {
			String res = result.getThrowable().getMessage();
			try {
				reporter.logSkipTest(driver, methodName, res);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		if (driver != null) {
			try {
				driver.manage().deleteAllCookies();
				logger.info("Cookies are deleted");
				driver.quit();
			} catch (CustomException e) {
				e.getLocalizedMessage();
				e.getMessage();
				logger.error("Custom Exception for tear down", e);
			} catch (Exception ex) {
				logger.error("tear Down", ex);
			}
		}
		else
			logger.error("Driver is null");
		
		reporter.endTest();	
		logger.info("*****************************************************************");
		logger.info("*****************************************************************");
	}

	/**
	 * This method is executed at the end of all tests are executed.
	 * @throws Exception 
	 * @throws Exception
	 */
	@AfterSuite(alwaysRun=true)
	public void shutdown() throws Exception {
		reporter.flush();
		//EmailReport.send_report();
	}

	private String getBrowserName() {
		RemoteWebDriver webDriver = (RemoteWebDriver) driver;
		return webDriver.getCapabilities().getBrowserName();
	}

	private String getBrowserVersion() {
		RemoteWebDriver webDriver = (RemoteWebDriver) driver;
		return webDriver.getCapabilities().getVersion();
	}

	private String getPlatform() {
		RemoteWebDriver webDriver = (RemoteWebDriver) driver;
		return webDriver.getCapabilities().getPlatform().name();
	}

	public String verifyResultsMap() {
		String result = null;
		for (Map.Entry<String, List<String>> map : resultMap.entrySet()) {
			if (!map.getValue().isEmpty()) {
				result += map.getKey() + "---->" + map.getValue() + "\n";
			}
		}
		return result;
	}
}
