package com.mst.automation.demo.utils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseUtility {

	static Statement stmnt = null;
	final String JDBC_DRIVER = TestUtils.getStringFromPropertyFile("jdbc.driver");
	final static String DB_URL = TestUtils.getStringFromPropertyFile("db.url");
	final static String USER = TestUtils.getStringFromPropertyFile("db.user");
	final static String PASS = TestUtils.getStringFromPropertyFile("db.password");
	static Connection conn = null;

	public static List<ArrayList<String>> getDBValues(String query) throws SQLException {

		System.getProperty("java.version");
		System.getProperty("sun.arch.data.model");
		List<ArrayList<String>> listOfLists = null;
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmnt = conn.createStatement();
			ResultSet rs = stmnt.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			for(int i=1;i<=rsmd.getColumnCount();i++) {
				
			}
			List<Row> table = new ArrayList<Row>();
			Row.formTable(rs, table);
			listOfLists = getTableValuesInListOfList(table);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfLists;
	}

	@SuppressWarnings("null")
	public static ArrayList<String> getDBMetadata(String query) throws SQLException {

		System.getProperty("java.version");
		System.getProperty("sun.arch.data.model");
		ArrayList<String> list = null;
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmnt = conn.createStatement();
			ResultSet rs = stmnt.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			String value;
			list = new ArrayList<String>();
			for(int i=1;i<=rsmd.getColumnCount();i++) {
				value = rsmd.getColumnName(i);
				list.add(value);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("rawtypes")
	public static List<ArrayList<String>> getTableValuesInListOfList(List<Row> table) {
		String value = null;
		List<ArrayList<String>> listOLists = new ArrayList<ArrayList<String>>();
		ArrayList<String> arraylist;

		for (Row row : table) {
			arraylist = new ArrayList<String>();
			for (Entry<Object, Class> col : row.row) {
				value = (col.getValue()).cast(col.getKey()).toString();
					arraylist.add(value);
			}
			listOLists.add(arraylist);
		}
		return listOLists;
	}
}

@SuppressWarnings("rawtypes")
class Row {

	public List<Entry<Object, Class>> row;

	public static Map<String, Class> TYPE;

	static {
		TYPE = new HashMap<String, Class>();

		TYPE.put("INT", Integer.class);
		TYPE.put("TINYINT", Byte.class);
		TYPE.put("SMALLINT", Short.class);
		TYPE.put("BIGINT", Long.class);
		TYPE.put("REAL", Float.class);
		TYPE.put("FLOAT", Double.class);
		TYPE.put("DOUBLE", Double.class);
		TYPE.put("DECIMAL", BigDecimal.class);
		TYPE.put("NUMERIC", BigDecimal.class);
		TYPE.put("BOOLEAN", Boolean.class);
		TYPE.put("CHAR", String.class);
		TYPE.put("VARCHAR", String.class);
		TYPE.put("LONGVARCHAR", String.class);
		TYPE.put("DATE", Date.class);
		TYPE.put("TIME", Time.class);
		TYPE.put("TIMESTAMP", Timestamp.class);
		TYPE.put("SERIAL", Integer.class);
	}


	public Row() {
		row = new ArrayList<Entry<Object, Class>>();
	}


	public <T> void add(T data) {
		row.add(new AbstractMap.SimpleImmutableEntry<Object, Class>(data, data.getClass()));
	}

	public void add(Object data, String sqlType) {
		Class<?> castType = Row.TYPE.get(sqlType.toUpperCase());
		try {
			this.add(castType.cast(data));
		} catch (NullPointerException e) {
			e.printStackTrace();
			Logger lgr = Logger.getLogger(Row.class.getName());
			lgr.log(Level.SEVERE,
					e.getMessage() + " Add the type " + sqlType + " to the TYPE hash map in the Row class.", e);
			throw e;
		}
	}

	public static void formTable(ResultSet rs, List<Row> table) throws SQLException {
		if (rs == null)
			return;

		ResultSetMetaData rsmd;
		try {
			rsmd = rs.getMetaData();

			int NumOfCol = rsmd.getColumnCount();

			while (rs.next()) {
				Row current_row = new Row();

				for (int i = 1; i <= NumOfCol; i++) {
					current_row.add(rs.getObject(i), rsmd.getColumnTypeName(i));
				}

				table.add(current_row);
			}
		} catch (SQLException e) {
			throw e;
		}
	}
}
